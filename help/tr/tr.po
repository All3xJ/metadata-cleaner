# Oğuz Ersen <oguzersen@protonmail.com>, 2021.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2021-10-24 23:48+0200\n"
"PO-Revision-Date: 2021-10-27 18:04+0000\n"
"Last-Translator: Oğuz Ersen <oguzersen@protonmail.com>\n"
"Language-Team: Turkish <https://hosted.weblate.org/projects/metadata-cleaner/"
"help/tr/>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.9-dev\n"

#. (itstool) path: section/title
#: C/general.page:14
msgid "Metadata and privacy"
msgstr "Üst veriler ve gizlilik"

#. (itstool) path: section/p
#: C/general.page:15
msgid ""
"Metadata consists of information that characterizes data. Metadata is used "
"to provide documentation for data products. In essence, metadata answers "
"who, what, when, where, why, and how about every facet of the data that is "
"being documented."
msgstr ""
"Üst veriler, verilerin kendisi hakkında ayırt edici bilgilerden oluşur. Üst "
"veriler, veri ürünleri için belgelendirme sağlamak için kullanılır. Özünde, "
"üst veriler belgelendirilen verilerin kim, ne, ne zaman, nerede, neden ve "
"nasıl gibi her yönü hakkındaki sorulara yanıt verir."

#. (itstool) path: section/p
#: C/general.page:16
msgid ""
"Metadata within a file can tell a lot about you. Cameras record data about "
"when and where a picture was taken and which camera was used. Office "
"applications automatically add author and company information to documents "
"and spreadsheets. This is sensitive information and you may not want to "
"disclose it."
msgstr ""
"Bir dosyadaki üst veriler sizin hakkınızda çok şey söyleyebilir. Kameralar, "
"bir resmin ne zaman ve nerede çekildiği ve hangi kameranın kullanıldığı ile "
"ilgili verileri kaydeder. Ofis uygulamaları, belgelere ve elektronik "
"tablolara otomatik olarak yazar ve şirket bilgilerini ekler. Bunlar hassas "
"bilgilerdir ve açıklamak istemeyebilirsiniz."

#. (itstool) path: figure/title
#: C/general.page:18
msgid "Example of metadata in a picture file"
msgstr "Bir resim dosyasındaki üst veri örneği"

#. (itstool) path: figure/desc
#: C/general.page:20
msgid ""
"The file discloses information about the hardware, settings, date, location "
"of the shoot. It contains a total of 79 metadata."
msgstr ""
"Dosyada yapılan çekimin donanımı, ayarları, tarihi, yeri ile ilgili bilgiler "
"açığa vuruluyor. Toplam 79 üst veri içeriyor."

#. (itstool) path: section/title
#: C/general.page:27
msgid "Cleaning process"
msgstr "Temizleme işlemi"

#. (itstool) path: section/p
#: C/general.page:28
msgid ""
"While <app>Metadata Cleaner</app> is doing its very best to display "
"metadata, it doesn't mean that a file is clean from metadata if it doesn't "
"show any. There is no reliable way to detect every single possible metadata "
"for complex file formats. This is why you shouldn't rely on metadata's "
"presence to decide if your file must be cleaned or not."
msgstr ""
"<app>Üst Veri Temizleyici</app>, üst verileri görüntülemek için elinden "
"gelenin en iyisini yapıyor olsa da, herhangi bir üst veri göstermiyorsa bu, "
"bir dosyanın üst verilerden tamamen temiz olduğu anlamına gelmez. Karmaşık "
"dosya biçimleri için olası her üst veriyi algılamanın güvenilir bir yolu "
"yoktur. Bu nedenle, dosyanızın temizlenmesi gerekip gerekmediğine karar "
"vermek için üst verilerin varlığına güvenmemelisiniz."

#. (itstool) path: section/p
#: C/general.page:29
msgid ""
"<app>Metadata Cleaner</app> takes the content of the file and puts it into a "
"new one without metadata, ensuring that any undetected metadata is stripped."
msgstr ""
"<app>Üst Veri Temizleyici</app>, dosyanın içeriğini alır ve üst veri "
"içermeyen yeni bir dosyaya yerleştirir ve algılanmayan üst verilerin "
"kaldırıldığından emin olunmasını sağlar."

#. (itstool) path: section/title
#: C/general.page:35
msgid "Limitations"
msgstr "Sınırlamalar"

#. (itstool) path: section/p
#: C/general.page:36
msgid ""
"Be aware that metadata is not the only way of marking a file. If the content "
"itself discloses personal information or has been watermarked, traditionally "
"or via steganography, <app>Metadata Cleaner</app> will not protect you."
msgstr ""
"Bir dosyayı işaretlemenin tek yolunun üst veriler olmadığını unutmayın. "
"İçeriğin kendisi kişisel bilgileri ortaya döküyorsa veya geleneksel olarak "
"ya da steganografi yoluyla filigran eklenmişse, <app>Üst Veri Temizleyici</"
"app> sizi korumayacaktır."

#. (itstool) path: page/title
#. The application name. It can be translated.
#: C/index.page:10
msgid "Metadata Cleaner"
msgstr "Üst Veri Temizleyici"

#. (itstool) path: page/p
#: C/index.page:14
msgid ""
"<app>Metadata Cleaner</app> allows you to view metadata in your files and to "
"get rid of it, as much as possible."
msgstr ""
"<app>Üst Veri Temizleyici</app>, dosyalarınızdaki üst verileri "
"görüntülemenizi ve mümkün olduğunca onlardan kurtulmanızı sağlar."

#. (itstool) path: section/title
#: C/index.page:17
msgid "General information"
msgstr "Genel bilgiler"

#. (itstool) path: section/title
#: C/index.page:21
msgid "Using <app>Metadata Cleaner</app>"
msgstr "<app>Üst Veri Temizleyiciyi</app> kullanma"

#. (itstool) path: section/title
#: C/usage.page:14
msgid "Adding files"
msgstr "Dosya ekleme"

#. (itstool) path: section/p
#: C/usage.page:15
msgid ""
"To add files, press the <gui style=\"button\">Add Files</gui> button. A file "
"chooser will open, select the files you want to clean."
msgstr ""
"Dosya eklemek için <gui style=\"button\">Dosya Ekle</gui> düğmesine basın. "
"Bir dosya seçici açılacaktır, temizlemek istediğiniz dosyaları seçin."

#. (itstool) path: figure/title
#: C/usage.page:17
msgid "<gui style=\"button\">Add Files</gui> button"
msgstr "<gui style=\"button\">Dosya Ekle</gui> düğmesi"

#. (itstool) path: section/p
#: C/usage.page:20
msgid ""
"To add whole folders at once, press the arrow next to the <gui style=\"button"
"\">Add Files</gui> button and press the <gui style=\"button\">Add Folders</"
"gui> button. A file chooser will open, select the folders you want to add. "
"You can optionally choose to also add all files from all subfolders by "
"checking the <gui style=\"checkbox\">Add files from subfolders</gui> "
"checkbox."
msgstr ""
"Tüm klasörleri bir kerede eklemek için, <gui style=\"button\">Dosya Ekle</"
"gui> düğmesinin yanındaki oka ve ardından <gui style=\"button\">Klasör Ekle</"
"gui> düğmesine basın. Bir dosya seçici açılacaktır, eklemek istediğiniz "
"klasörleri seçin. İsteğe bağlı olarak, <gui style=\"checkbox\">Alt "
"klasörlerden dosya ekle</gui> onay kutusunu işaretleyerek tüm alt "
"klasörlerdeki tüm dosyaları eklemeyi de seçebilirsiniz."

#. (itstool) path: section/title
#: C/usage.page:26
msgid "Viewing metadata"
msgstr "Üst verileri görüntüleme"

#. (itstool) path: section/p
#: C/usage.page:27
msgid ""
"Click on a file in the list to open the detailed view. If it has metadata, "
"it will be shown there."
msgstr ""
"Ayrıntılı görünümü açmak için listedeki bir dosyaya tıklayın. Üst verileri "
"varsa, orada gösterilecektir."

#. (itstool) path: figure/title
#: C/usage.page:29
msgid "Detailed view of the metadata"
msgstr "Üst verilerin ayrıntılı görünümü"

#. (itstool) path: section/title
#: C/usage.page:37
msgid "Cleaning files"
msgstr "Dosyaları temizleme"

#. (itstool) path: section/p
#: C/usage.page:38
msgid ""
"To clean all the files in the window, press the <gui style=\"button\">Clean</"
"gui> button. The cleaning process may take some time to complete."
msgstr ""
"Penceredeki tüm dosyaları temizlemek için <gui style=\"button\">Temizle</"
"gui> düğmesine basın. Temizleme işleminin tamamlanması biraz zaman alabilir."

#. (itstool) path: figure/title
#: C/usage.page:40
msgid "<gui style=\"button\">Clean</gui> button"
msgstr "<gui style=\"button\">Temizle</gui> düğmesi"

#. (itstool) path: section/title
#: C/usage.page:48
msgid "Lightweight cleaning"
msgstr "Hafif temizleme"

#. (itstool) path: section/p
#: C/usage.page:49
msgid ""
"By default, the removal process might alter a bit the data of your files, in "
"order to remove as much metadata as possible. For example, texts in PDF "
"might not be selectable anymore, compressed images might get compressed "
"again…"
msgstr ""
"Öntanımlı olarak, mümkün olduğunca fazla üst veriyi kaldırmak için kaldırma "
"işlemi dosyalarınızın verilerini biraz değiştirebilir. Örneğin, PDF "
"dosyalarındaki metinler artık seçilemeyebilir, sıkıştırılmış resimler tekrar "
"sıkıştırılabilir…"

#. (itstool) path: section/p
#: C/usage.page:50
msgid ""
"The lightweight mode, while not as thorough, will not make destructive "
"changes to your files."
msgstr ""
"Hafif mod, o kadar kapsamlı olmasa da dosyalarınızda yıkıcı değişiklikler "
"yapmayacaktır."

#~ msgid ""
#~ "If you're willing to trade some metadata's presence in exchange of the "
#~ "guarantee that the data of your files won't be modified, the lightweight "
#~ "mode precisely does that."
#~ msgstr ""
#~ "Dosyalarınızın verilerinin değiştirilmeyeceği garantisi karşılığında bazı "
#~ "üst verilerin varlığını takas etmek istiyorsanız, hafif mod tam olarak "
#~ "bunu yapar."
