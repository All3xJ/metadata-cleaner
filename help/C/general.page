<?xml version="1.0" encoding="UTF-8"?>
<!--
SPDX-FileCopyrightText: 2021 Romain Vigier <contact AT romainvigier.fr>
SPDX-License-Identifier: CC-BY-SA-4.0
-->
<page xmlns="http://projectmallard.org/1.0/"
	xmlns:its="http://www.w3.org/2005/11/its"
	type="topic"
	id="general">
	<section id="metadata">
		<info>
			<link type="guide" xref="index#general"/>
		</info>
		<title>Metadata and privacy</title>
		<p>Metadata consists of information that characterizes data. Metadata is used to provide documentation for data products. In essence, metadata answers who, what, when, where, why, and how about every facet of the data that is being documented.</p>
		<p>Metadata within a file can tell a lot about you. Cameras record data about when and where a picture was taken and which camera was used. Office applications automatically add author and company information to documents and spreadsheets. This is sensitive information and you may not want to disclose it.</p>
		<figure>
			<title>Example of metadata in a picture file</title>
			<media type="image" src="figures/metadata-example.png" its:translate="no"/>
			<desc>The file discloses information about the hardware, settings, date, location of the shoot. It contains a total of 79 metadata.</desc>
		</figure>
	</section>
	<section id="cleaning-process">
		<info>
			<link type="guide" xref="index#general"/>
		</info>
		<title>Cleaning process</title>
		<p>While <app>Metadata Cleaner</app> is doing its very best to display metadata, it doesn't mean that a file is clean from metadata if it doesn't show any. There is no reliable way to detect every single possible metadata for complex file formats. This is why you shouldn't rely on metadata's presence to decide if your file must be cleaned or not.</p>
		<p><app>Metadata Cleaner</app> takes the content of the file and puts it into a new one without metadata, ensuring that any undetected metadata is stripped.</p>
	</section>
	<section id="limitations">
		<info>
			<link type="guide" xref="index#general"/>
		</info>
		<title>Limitations</title>
		<p>Be aware that metadata is not the only way of marking a file. If the content itself discloses personal information or has been watermarked, traditionally or via steganography, <app>Metadata Cleaner</app> will not protect you.</p>
	</section>
</page>
